; Author: Joe Nyugoh Ngigi , Magnum Digital Limited Kenya

;---------------------
;Include Modern UI
   !include "MUI.nsh"
   ; !include "MUI2.nsh"
;--------------------------------


  SetCompressor /solid lzma
  XPStyle on
  ; HM NIS Edit Wizard helper defines
  !define PRODUCT_NAME "Biashara Retail POS"
  !define PRODUCT_VERSION "1.1.0"
  !define PRODUCT_PUBLISHER "Magnum Digital Limited Kenya"
  !define PRODUCT_WEB_SITE "http://www.biasharapos.com"
  !define WORK_DIR "C:\Users\joe\Desktop\BAMP"
  Caption "${PRODUCT_NAME} ${PRODUCT_VERSION}"
  InstallDirRegKey HKLM "Software\bamp" "Install_Dir"
  Name "${PRODUCT_NAME} ${PRODUCT_VERSION}"
  OutFile "${WORK_DIR}\dist\${PRODUCT_NAME}-${PRODUCT_VERSION}.exe"
  RequestExecutionLevel admin
  BGGradient off
  BrandingText "${PRODUCT_NAME}-v${PRODUCT_VERSION}"
;--------------------------------

;Remember the installer language
  !define MUI_LANGDLL_REGISTRY_ROOT "HKLM"
  !define MUI_LANGDLL_REGISTRY_KEY "Software\bamp"
  !define MUI_LANGDLL_REGISTRY_VALUENAME "lang"

  !define MUI_ABORTWARNING
  !define MUI_ICON "${WORK_DIR}\src\icons\cashier.ico"
  !define MUI_UNICON "${WORK_DIR}\src\icons\cashier.ico"
  !define MUI_WELCOMEPAGE

;Pages
  !insertmacro MUI_PAGE_WELCOME
  !insertmacro MUI_PAGE_INSTFILES
  !insertmacro MUI_PAGE_FINISH

  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH

;Resevefiles
  ;!insertmacro MUI_RESERVEFILE_LANGDLL
  ;!insertmacro MUI_RESERVEFILE_INSTALLOPTIONS

;Languages
  !insertmacro MUI_LANGUAGE "English" # first language is the default language

;--------------------------------

InstallDir "C:\bamp"
Icon "${WORK_DIR}\src\icons\cashier.ico"
UninstallIcon "${WORK_DIR}\src\icons\cashier.ico"
ShowInstDetails show
ShowUninstDetails show

;Fuctions
Function .onInit

!insertmacro MUI_LANGDLL_DISPLAY

; Missing MS C++ 2008 runtime library warning here
  ReadRegStr $R2 HKLM 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{FF66E9F6-83E7-3A3E-AF14-8DE9A809A6A4}' DisplayVersion
  ReadRegStr $R3 HKLM 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{350AA351-21FA-3270-8B7A-835434E766AD}' DisplayVersion
  ReadRegStr $R4 HKLM 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{2B547B43-DB50-3139-9EBE-37D419E0F5FA}' DisplayVersion

  ReadRegStr $R5 HKLM 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{9A25302D-30C0-39D9-BD6F-21E6EC160475}' DisplayVersion
  ReadRegStr $R6 HKLM 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{8220EEFE-38CD-377E-8595-13398D740ACE}' DisplayVersion
  ReadRegStr $R7 HKLM 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{5827ECE1-AEB0-328E-B813-6FC68622C1F9}' DisplayVersion

  ReadRegStr $R8 HKLM 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{1F1C2DFC-2D24-3E06-BCB8-725134ADF989}' DisplayVersion
  ReadRegStr $R9 HKLM 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{4B6C7001-C7D6-3710-913E-5BC23FCE91E6}' DisplayVersion
  ReadRegStr $R0 HKLM 'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{977AD349-C2A8-39DD-9273-285C08987C7B}' DisplayVersion


  ; VC 2010
  ;ReadRegStr $A1 HKLM 'SOFTWARE\Microsoft\VisualStudio\10.0\VC\VCRedist\x86' Installed
  ;ReadRegStr $A2 HKLM 'SOFTWARE\Microsoft\VisualStudio\10.0\VC\VCRedist\x64' Installed
  ;ReadRegStr $A3 HKLM 'SOFTWARE\Microsoft\VisualStudio\10.0\VC\VCRedist\ia64' Installed
  ;ReadRegStr $A4 HKLM 'SOFTWARE\Wow6432Node\Microsoft\VisualStudio\10.0\VC\VCRedist\x64' Installed

  StrCmp $R2 "" vc9_test2
  GOTO init_end
  vc9_test2:
  StrCmp $R3 "" vc9_test3
  GOTO init_end
  vc9_test3:
  StrCmp $R4 "" vc9_test4
  GOTO init_end
  vc9_test4:
  StrCmp $R5 "" vc9_test5
  GOTO init_end
  vc9_test5:
  StrCmp $R6 "" vc9_test6
  GOTO init_end
  vc9_test6:
  StrCmp $R7 "" vc9_test7
  GOTO init_end
  vc9_test7:
  StrCmp $R8 "" vc9_test8
  GOTO init_end
  vc9_test8:
  StrCmp $R9 "" vc9_test9
  GOTO init_end
  vc9_test9:
  StrCmp $R0 "" no_vc9
  GOTO init_end

  no_vc9:
        StrCmp $LANGUAGE "1031" lang_de
               MessageBox MB_YESNO "Warning: ${PRODUCT_NAME} cannot work without the Microsoft Visual C++ 2008 Redistributable Package. Now open the Microsoft page for this download?" IDNO MsPageOut
               ;MessageBox MB_YESNO "MS C++ 2008 runtime package not found! It is required for PHP. Now open the Microsoft site for this download?" IDNO MsPageOut
               ExecShell "open" "http://www.microsoft.com/en-us/download/details.aspx?id=5582"
               GOTO MsPageOut
               lang_de:
               MessageBox MB_YESNO "Warnung: ${PRODUCT_NAME} ben�tigt das Microsoft Visual C++ 2008 Redistributable Package! Die Microsoft Seite f�r diesen Download jetzt �ffnen?" IDNO MsPageOut
               ; MessageBox MB_YESNO "MS C++ 2008 runtime Installation fehlt auf Ihrem Rechner! Diese wird f�r PHP ben�tigt. Die Microsoft Seite f�r diesen Download jetzt �ffnen?" IDNO MsPageOut
               ExecShell "open" "http://www.microsoft.com/en-us/download/details.aspx?id=5582"
        MsPageOut:
               ; StrCmp $LANGUAGE "1031" lang_de2
               ; MessageBox MB_YESNO "Perhaps ${PRODUCT_NAME} do not work without the MS VC++ 2008 runtime library. Still go on with the POS installation?" IDNO GoOut
               ; GOTO init_end
               ; lang_de2:
               ; MessageBox MB_YESNO "Ohne die MS VC++ 2008 Runtime Bibliothek k�nnte XAMPP nicht funktionieren! Die XAMPP Installation trotzdem weiterf�hren?" IDNO GoOut
               ; GOTO init_end
               ; GoOut:
               ; Abort "Exit by user."
  init_end:
FunctionEnd

Function un.onInit
!insertmacro MUI_LANGDLL_DISPLAY
;!insertmacro MUI_UNGETLANGUAGE
FunctionEnd

Function WriteToFile
Exch $0 ;file to write to
Exch
Exch $1 ;text to write

  FileOpen $0 $0 a #open file
  FileSeek $0 0 END #go to end
  FileWrite $0 $1 #write to file
  FileClose $0

Pop $1
Pop $0
FunctionEnd

!macro WriteToFile NewLine File String
  !if `${NewLine}` == true
  Push `${String}$\r$\n`
  !else
  Push `${String}`
  !endif
  Push `${File}`
  Call WriteToFile
!macroend
!define WriteToFile `!insertmacro WriteToFile false`
!define WriteLineToFile `!insertmacro WriteToFile true`


Function .onInstSuccess

StrCmp $LANGUAGE "1031" detection_de
GOTO no_de
detection_de:
no_de:

WriteUninstaller "$INSTDIR\Uninstall.exe"
FunctionEnd

Section "-XAMPP Files"
SetOverwrite ifnewer
SetOutPath "$INSTDIR"
File "${WORK_DIR}\start.bat"

SetOutPath "$INSTDIR\cgi-bin"
File /r "${WORK_DIR}\cgi-bin\*.*"
SetOutPath "$INSTDIR\htdocs"
File /r "${WORK_DIR}\htdocs\*.*"
SetOutPath "$INSTDIR\licenses"
File /r "${WORK_DIR}\licenses\*.*"
SetOutPath "$INSTDIR\locale"
File /r "${WORK_DIR}\locale\*.*"
SetOutPath "$INSTDIR\tmp"
File /r "${WORK_DIR}\tmp\*.*"
SetOutPath "$INSTDIR\nodejs"
File /r "${WORK_DIR}\nodejs\*.*"
SetOutPath "$INSTDIR\src\icons"
File /r "${WORK_DIR}\src\icons\*.*"

WriteRegStr HKLM "Software\bamp" "Install_Dir" "$INSTDIR"
WriteRegStr HKLM "Software\bamp" "apache" "2430"
WriteRegStr HKLM "Software\bamp" "version" "1810"
WriteRegStr HKLM "Software\bamp" "lang" "$LANGUAGE"
WriteRegStr HKLM "Software\bamp" "programfiles" "0"
WriteRegStr HKLM "Software\bamp" "desktopicon" "0"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\bamp" "DisplayName" "${PRODUCT_NAME} ${PRODUCT_VERSION}"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\bamp" "UninstallString" '"$INSTDIR\uninstall.exe"'
WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\bamp" "NoModify" 1
WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\bamp" "NoRepair" 1
SectionEnd

Section "${PRODUCT_NAME} Start Menu"
CreateDirectory "$SMPROGRAMS\${PRODUCT_NAME}"
CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}" "" ""
CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}\${PRODUCT_NAME}.lnk" "$INSTDIR\${PRODUCT_NAME}.exe" "" "$INSTDIR\src\icons\cashier.ico"
CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\src\icons\cashier.ico"
WriteRegStr HKLM "Software\bamp" "programfiles" "1"
SectionEnd

Section "BAMP Desktop Icon"
CreateShortCut "$DESKTOP\${PRODUCT_NAME}.lnk" "$INSTDIR\${PRODUCT_NAME}.exe" ""
WriteRegStr HKLM "Software\bamp" "desktopicon" "1"
SectionEnd

Section "Apache"
SetOverwrite ifnewer
SetOutPath "$INSTDIR\apache"
File /r "${WORK_DIR}\apache\*.*"
SectionIn RO
SectionEnd

Section "MySQL"
SetOverwrite ifnewer
SetOutPath "$INSTDIR\mysql"
File /r "${WORK_DIR}\mysql-bamp\*.*"
SectionEnd

Section "PHP"
SetOverwrite ifnewer
SetOutPath "$INSTDIR\php"
File /r "${WORK_DIR}\php\*.*"
SectionIn RO
SectionEnd

Section "Electron"
SetOverwrite ifnewer
SetOutPath "$INSTDIR"
File /r "${WORK_DIR}\electron\dist\win-ia32-unpacked\*.*"
SectionIn RO
SectionEnd

Section "Uninstall"

;ReadRegStr $9 HKLM "Software\bamp" "lang"
;StrCmp $9 "1031" german0
;MessageBox MB_YESNO "Really uninstall ${PRODUCT_NAME}?" IDYES NoAbort1
;Abort
;NoAbort1:
;GOTO mess_out
;german0:
;MessageBox MB_YESNO "Wollen Sie wirklich ${PRODUCT_NAME} desinstallieren?" IDYES mess_out
;Abort
;mess_out:

ReadRegStr $0 HKLM "Software\bamp" "desktopicon"
StrCmp $0 "0" no_icon
Delete "$DESKTOP\${PRODUCT_NAME}.lnk"
no_icon:
ReadRegStr $8 HKLM "Software\bamp" "programfiles"
StrCmp $8 "0" no_pfiles
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\*.*"
  RMDir /r "$SMPROGRAMS\${PRODUCT_NAME}"
no_pfiles:

RMDir /r "$INSTDIR\apache"
RMDir /r "$INSTDIR\licenses"
RMDir /r "$INSTDIR\php"
RMDir /r "$INSTDIR\phpMyAdmin"
RMDir /r "$INSTDIR\tmp"
RMDir /r "$INSTDIR\src"
RMDir /r "$INSTDIR\locale"
RMDir /r "$INSTDIR\locales"
RMDir /r "$INSTDIR\mysql"
RMDir /r "$INSTDIR\resources"
RMDir /r "$INSTDIR\nodejs"
RMDir /r "$INSTDIR\htdocs"
RMDir /r "$INSTDIR\cgi-bin"
Delete "$INSTDIR\*.*"
RMDir /r "$INSTDIR"


DeleteRegKey HKLM "Software\bamp"
DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\bamp"

SectionEnd
